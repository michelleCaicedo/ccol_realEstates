package com.bbva.ccol.realestates.properties;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

@Component(value = "realEstatesProperties")
public class RealEstatesProperties {
	@Resource(name = "realEstatesBean")
	private Properties realEstatesBean;
	private static I18nLog log = I18nLogFactory.getLogI18n(
			RealEstatesProperties.class,
			"META-INF/spring/i18n/log/mensajesLog");
	
	public String getProperty(String key) {
		String propiedad = this.realEstatesBean.getProperty(key);
		return propiedad;
	}
}

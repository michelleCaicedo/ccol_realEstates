package com.bbva.ccol.realestates.facade.v01.mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.bbva.ccol.realestates.business.dto.DTOIntAmount;
import com.bbva.ccol.realestates.business.dto.DTOIntAverageAmountsSummaryItem;
import com.bbva.ccol.realestates.business.dto.DTOIntAverageAmountsSummaryType;
import com.bbva.ccol.realestates.business.dto.DTOIntFinancingData;
import com.bbva.ccol.realestates.business.dto.DTOIntGeographicGroup;
import com.bbva.ccol.realestates.business.dto.DTOIntIncome;
import com.bbva.ccol.realestates.business.dto.DTOIntIncomeType;
import com.bbva.ccol.realestates.business.dto.DTOIntIncomeValue;
import com.bbva.ccol.realestates.business.dto.DTOIntLocation;
import com.bbva.ccol.realestates.business.dto.DTOIntPriceSnapshot;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstateType;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstateValuations;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstates;
import com.bbva.ccol.realestates.business.dto.ValuationsMadiva;
import com.bbva.ccol.realestates.business.dto.ZoneDetailMadiva;
import com.bbva.ccol.realestates.business.dto.ZoneInfoMadiva;
import com.bbva.ccol.realestates.business.dto.comparable;
import com.bbva.ccol.realestates.business.dto.department_list;
import com.bbva.ccol.realestates.business.dto.house_list;
import com.bbva.ccol.realestates.business.dto.precios;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstates;
import com.bbva.ccol.realestates.facade.v01.dto.RealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.RealEstates;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(Mapper.class,"META-INF/spring/i18n/log/mensajesLog");

	public static List<ZoneInfoMadiva> jsonToListRealEstates(String jsonRealEstates) {	
		 List<ZoneInfoMadiva> listZoneInfoMadiva = new  ArrayList<ZoneInfoMadiva>();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			listZoneInfoMadiva = objectMapper.readValue(jsonRealEstates.toLowerCase(),new TypeReference<List<ZoneInfoMadiva>>() {});		
			String json = objectMapper.writeValueAsString(listZoneInfoMadiva);
			log.debug("pruebas michelita"+ json);
		} catch (Exception e) {
		log.error("Error mapeo json to dtoINT jsonToListRealEstates" + e.getMessage());
		}		
		return listZoneInfoMadiva;
	}
	public static ZoneDetailMadiva jsonToZoneDetailMadiva(String jsonRealEstates) {
		
		ZoneDetailMadiva zoneDetailMadiva = new ZoneDetailMadiva();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
		 zoneDetailMadiva = objectMapper.readValue(jsonRealEstates,new TypeReference<ZoneDetailMadiva>() {});
		} catch (Exception e) {
		log.error("Error mapeo json to dtoINT jsonToZoneDetailMadiva" + e.getMessage());
		}
		return zoneDetailMadiva;
	}
	
	public static ValuationsMadiva jsonToListRealEstateValuations(String json){
		ValuationsMadiva valuationsMadiva= new ValuationsMadiva();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
			try {
				valuationsMadiva = objectMapper.readValue(json.toLowerCase(),new TypeReference<ValuationsMadiva>() {});
			} catch (Exception e) {
			log.error("Error mapeo json to jsonToListRealEstateValuations " + e.getMessage());	
		}
			return valuationsMadiva;
	}
	
	public static List<DTOIntRealEstateValuations> dtoExtToDtoIntListRealEstateValuations(ValuationsMadiva valuationsMadiva){
		List<DTOIntRealEstateValuations> listDTOIntRealEstateValuations= new ArrayList<DTOIntRealEstateValuations>();
		
		DTOIntRealEstateValuations DTOIntRealEstateValuations = new DTOIntRealEstateValuations();
				
				DTOIntRealEstateValuations.setRadius(valuationsMadiva.getAvgmetros());
				DTOIntRealEstateValuations.setPrecision(valuationsMadiva.getPrecision());	
				DTOIntAmount price= new DTOIntAmount ();
				price.setAmount(valuationsMadiva.getPrecioventa());
				price.setCurrency("COP");
				DTOIntRealEstateValuations.setPrice(price);
				List<DTOIntAverageAmountsSummaryItem> listAverageAmountsSummary= new ArrayList<DTOIntAverageAmountsSummaryItem>();
				//
				DTOIntAverageAmountsSummaryItem averageAmountsSummary= new DTOIntAverageAmountsSummaryItem();
				DTOIntAmount averageAmount= new DTOIntAmount ();
				averageAmount.setAmount(valuationsMadiva.getAvgprecio());
				averageAmount.setCurrency("COP");
				averageAmountsSummary.setAverageAmount(averageAmount);
				DTOIntAverageAmountsSummaryType averageAmountsSummaryType= new DTOIntAverageAmountsSummaryType ();
				averageAmountsSummaryType.setId("AVERAGE_AMOUNT_PRICE");
				averageAmountsSummary.setAverageAmountsSummaryType(averageAmountsSummaryType);
				listAverageAmountsSummary.add(averageAmountsSummary);
				//
				DTOIntAverageAmountsSummaryItem averageAmountsSummary2= new DTOIntAverageAmountsSummaryItem();
				DTOIntAmount averageAmountM2= new DTOIntAmount ();
				averageAmountM2.setAmount(valuationsMadiva.getAvgpreciom2());
				averageAmountM2.setCurrency("COP");
				averageAmountsSummary2.setAverageAmount(averageAmountM2);
				DTOIntAverageAmountsSummaryType averageAmountsSummaryType2= new DTOIntAverageAmountsSummaryType ();
				averageAmountsSummaryType2.setId("AVERAGE_AMOUNT_PRICE_M2");
				averageAmountsSummary2.setAverageAmountsSummaryType(averageAmountsSummaryType2);				
				listAverageAmountsSummary.add(averageAmountsSummary2);
				//
				DTOIntAverageAmountsSummaryItem averageAmountsSummaryRent= new DTOIntAverageAmountsSummaryItem();
				DTOIntAmount averageAmountRent= new DTOIntAmount ();
				averageAmountRent.setAmount(valuationsMadiva.getPrecio_arriendo());
				averageAmountRent.setCurrency("COP");
				averageAmountsSummaryRent.setAverageAmount(averageAmountRent);
				DTOIntAverageAmountsSummaryType averageAmountsSummaryTypeRent= new DTOIntAverageAmountsSummaryType ();
				averageAmountsSummaryTypeRent.setId("AVERAGE_AMOUNT_RENT");
				averageAmountsSummaryRent.setAverageAmountsSummaryType(averageAmountsSummaryTypeRent);				
				listAverageAmountsSummary.add(averageAmountsSummaryRent);
				//
				DTOIntAverageAmountsSummaryItem averageAmountsSummaryRentm2= new DTOIntAverageAmountsSummaryItem();
				DTOIntAmount averageAmountRentm2= new DTOIntAmount ();
				averageAmountRentm2.setAmount(valuationsMadiva.getPrecio_arriendo_m2());
				averageAmountRentm2.setCurrency("COP");
				averageAmountsSummaryRentm2.setAverageAmount(averageAmountRentm2);
				DTOIntAverageAmountsSummaryType averageAmountsSummaryTypeRentm2= new DTOIntAverageAmountsSummaryType ();
				averageAmountsSummaryTypeRentm2.setId("AVERAGE_AMOUNT_RENT_M2");
				averageAmountsSummaryRentm2.setAverageAmountsSummaryType(averageAmountsSummaryTypeRentm2);				
				listAverageAmountsSummary.add(averageAmountsSummaryRentm2);
				//
				DTOIntRealEstateValuations.setAverageAmountsSummary(listAverageAmountsSummary);
				DTOIntFinancingData DTOIntFinancingData = new DTOIntFinancingData(); 
				DTOIntIncome estimatedIncome= new DTOIntIncome ();
				DTOIntIncomeType incomeType= new DTOIntIncomeType ();
				incomeType.setId("FIXED");
				estimatedIncome.setIncomeType(incomeType);
				DTOIntIncomeValue incomeValues= new DTOIntIncomeValue();
				incomeValues.setAmount(valuationsMadiva.getIngresos_minimos());
				incomeValues.setCurrency("COP");
				incomeValues.setFrequency("ANNUALLY");
				estimatedIncome.setIncomeValues(incomeValues);
				DTOIntAmount financingAmount2= new DTOIntAmount();
				financingAmount2.setAmount(valuationsMadiva.getAvgprecio());
				financingAmount2.setCurrency("COP");
				DTOIntFinancingData.setFinancingAmount(financingAmount2);
				DTOIntFinancingData.setEstimatedIncome(estimatedIncome);
				DTOIntAmount financingAmount= new DTOIntAmount();
				financingAmount.setAmount(valuationsMadiva.getCuota_hipoteca());
				financingAmount.setCurrency("COP");
				DTOIntFinancingData.setInstallmentAmount(financingAmount);
				DTOIntRealEstateValuations.setFinancingData(DTOIntFinancingData);
				
				List<DTOIntPriceSnapshot> listPriceSnapshots= new ArrayList<DTOIntPriceSnapshot>();
				if (valuationsMadiva.getHistorico_precio().getPrecios()!=null){
					for( precios listprecios:valuationsMadiva.getHistorico_precio().getPrecios()){
						DTOIntPriceSnapshot priceSnapshots= new DTOIntPriceSnapshot();	
						Calendar fecha = Calendar.getInstance();
						if(validateNullOrEmpty(listprecios.getYear())&&validateNullOrEmpty(listprecios.getMes())){
							fecha.set(Integer.parseInt(listprecios.getYear()),Integer.parseInt(listprecios.getMes())-1,1); //La hora no me interesa y recuerda que los meses van de 0 a 11
							priceSnapshots.setMonth(fecha.getTime());
						}
						DTOIntAmount price2= new DTOIntAmount(); 
						price2.setAmount(listprecios.getPrecio_m2());
						priceSnapshots.setPrice(price2);
						priceSnapshots.setPercentage(valuationsMadiva.getHistorico_precio().getIncremento());
						listPriceSnapshots.add(priceSnapshots);				
					} 
				}
				DTOIntRealEstateValuations.setPriceSnapshots(listPriceSnapshots);
				DTOIntLocation location= new DTOIntLocation();
				DTOIntGeographicGroup geographicGroup=new DTOIntGeographicGroup (); 
				geographicGroup.setCode(valuationsMadiva.getLatitud()+"*"+valuationsMadiva.getLongitud());
				location.setGeographicGroup(geographicGroup);
				DTOIntRealEstateValuations.setLocation(location);
				listDTOIntRealEstateValuations.add(DTOIntRealEstateValuations);
			
			return listDTOIntRealEstateValuations;
	}
	 
	public static List<DTOIntRealEstates> dtoExtToDtoIntListRealEstatesInfoZone(List<ZoneInfoMadiva> listZoneInfoMadiva) {
		
		List<DTOIntRealEstates> listDTOIntRealEstates = new ArrayList<DTOIntRealEstates>();
		
		try {
			for(ZoneInfoMadiva zoneInfoMadiva : listZoneInfoMadiva){
				if(zoneInfoMadiva.getHouse_list()!=null){
				for(house_list houseListTemp : zoneInfoMadiva.getHouse_list()){
					
					for(comparable compareblesTemp : houseListTemp.getComparables())
					{
						DTOIntRealEstates DTOIntRealEstates = new DTOIntRealEstates();
						
						DTOIntRealEstates.setId(compareblesTemp.getId());
						DTOIntRealEstates.setBathroomsNumber(compareblesTemp.getBathroom());
						DTOIntRealEstates.setSurface(compareblesTemp.getArea_m2());
						DTOIntRealEstates.setDescription(compareblesTemp.getDescription());
						DTOIntRealEstates.setImageUrl(compareblesTemp.getImg_url());
						DTOIntRealEstates.setParkingsNumber(compareblesTemp.getParking());
						DTOIntAmount DTOIntAmount= new DTOIntAmount();
						DTOIntAmount.setCurrency("COP");
						DTOIntAmount.setAmount(compareblesTemp.getPrice());
						DTOIntRealEstates.setPrice(DTOIntAmount);
						DTOIntRealEstates.setPropertyInformationWeb(compareblesTemp.getLinkportal());
						DTOIntAmount DTOIntAmount2= new DTOIntAmount();
						DTOIntAmount2.setAmount(compareblesTemp.getPrice_m2());
						DTOIntAmount2.setCurrency("COP");
						DTOIntRealEstates.setPriceByMeter(DTOIntAmount2);
						DTOIntLocation DTOIntLocation= new DTOIntLocation();
						DTOIntLocation.setAddressName(compareblesTemp.getAddress());
						DTOIntGeographicGroup geographicGroup=new DTOIntGeographicGroup (); 
						geographicGroup.setCode(compareblesTemp.getLatitude()+"*"+compareblesTemp.getLongitude());
						DTOIntLocation.setGeographicGroup(geographicGroup);
						DTOIntRealEstates.setLocation(DTOIntLocation);
						DTOIntRealEstates.setRoomsNumber(compareblesTemp.getRooms());
						DTOIntRealEstateType DTOIntRealEstateType= new DTOIntRealEstateType(); 
						DTOIntRealEstateType.setId("HOUSE");
						DTOIntRealEstates.setRealEstateType(DTOIntRealEstateType);
						DTOIntRealEstates.setAverageSurface(houseListTemp.getSurface());
						
						listDTOIntRealEstates.add(DTOIntRealEstates);
					}
			}
				} 
				if(zoneInfoMadiva.getDepartment_list()!=null){
				//Listado de apartamentos
				for (department_list department_listTemp : zoneInfoMadiva.getDepartment_list()) {
					for (comparable compareblesTemp : department_listTemp.getComparables()) {
						DTOIntRealEstates DTOIntRealEstates = new DTOIntRealEstates();

						DTOIntRealEstates.setId(compareblesTemp.getId());
						DTOIntRealEstates.setBathroomsNumber(compareblesTemp.getBathroom());
						DTOIntRealEstates.setSurface(compareblesTemp.getArea_m2());
						DTOIntRealEstates.setDescription(compareblesTemp.getDescription());
						DTOIntRealEstates.setImageUrl(compareblesTemp.getImg_url());
						DTOIntRealEstates.setParkingsNumber(compareblesTemp.getParking());
						DTOIntAmount DTOIntAmount= new DTOIntAmount();
						DTOIntAmount.setCurrency("COP");
						DTOIntAmount.setAmount(compareblesTemp.getPrice());
						DTOIntRealEstates.setPrice(DTOIntAmount);
						DTOIntRealEstates.setPropertyInformationWeb(compareblesTemp.getLinkportal());
						DTOIntAmount DTOIntAmount2= new DTOIntAmount();
						DTOIntAmount2.setAmount(compareblesTemp.getPrice_m2());
						DTOIntAmount2.setCurrency("COP");
						DTOIntRealEstates.setPriceByMeter(DTOIntAmount2);
						DTOIntLocation DTOIntLocation= new DTOIntLocation();
						DTOIntGeographicGroup geographicGroup=new DTOIntGeographicGroup (); 
						geographicGroup.setCode(compareblesTemp.getLatitude()+"*"+compareblesTemp.getLongitude());
						DTOIntLocation.setGeographicGroup(geographicGroup);
						DTOIntLocation.setAddressName(compareblesTemp.getAddress());
						DTOIntRealEstates.setLocation(DTOIntLocation);
						DTOIntRealEstates.setRoomsNumber(compareblesTemp.getRooms());
						DTOIntRealEstateType DTOIntRealEstateType= new DTOIntRealEstateType(); 
						DTOIntRealEstateType.setId("APARTMENT");
						DTOIntRealEstates.setRealEstateType(DTOIntRealEstateType);
						DTOIntRealEstates.setAverageSurface(department_listTemp.getSurface());
						listDTOIntRealEstates.add(DTOIntRealEstates);
					}
				}
				}
			}			
		} catch (Exception e) {
		log.error("Error mapeo json to dtoINT dtoExtToDtoIntListRealEstatesInfoZone " + e.getMessage());
		}
		return listDTOIntRealEstates;
	}


	public static List<DTOIntRealEstates> dtoExtToDtoIntListRealEstatesValDir(ZoneDetailMadiva zoneDetailMadiva) {
		
		List<DTOIntRealEstates> listDTOIntRealEstates = new ArrayList<DTOIntRealEstates>();
		DTOIntRealEstates DTOIntRealEstates = new DTOIntRealEstates();
		
		try {
			DTOIntLocation DTOIntLocation= new DTOIntLocation();
			DTOIntLocation.setAddressName(zoneDetailMadiva.getVia()+ " "+zoneDetailMadiva.getNumero());
			DTOIntLocation.setZipCode(zoneDetailMadiva.getCp());
			DTOIntGeographicGroup geographicGroup= new DTOIntGeographicGroup();
			geographicGroup.setCode(zoneDetailMadiva.getLatitud()+"*"+zoneDetailMadiva.getLongitud());
			DTOIntLocation.setGeographicGroup(geographicGroup);
			DTOIntRealEstates.setLocation(DTOIntLocation);
			listDTOIntRealEstates.add(DTOIntRealEstates);
		} catch (Exception e) {
		log.error("Error mapeo json to dtoExtToDtoIntListRealEstatesValDir" + e.getMessage());
		}
		return listDTOIntRealEstates;
	}
	
	public static List<RealEstates> dtoIntRealEstatesToDtoExt(List<DTOIntRealEstates> listDTOIntRealEstates){
		List<RealEstates> listRealEstates= new ArrayList<RealEstates>();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			String jsonString = objectMapper.writeValueAsString(listDTOIntRealEstates);
			listRealEstates = objectMapper.readValue(jsonString,new TypeReference<List<RealEstates>>() {});
			
		} catch (JsonProcessingException e) {
			log.error("Error dtoIntRealEstatesToDtoExt Json" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error dtoIntRealEstatesToDtoExt" + e.getMessage());
		}
		return listRealEstates;
	} 
	
	public static List<RealEstateValuations> dtoIntRealEstateValuationsToDtoExt(List<DTOIntRealEstateValuations> listDTOIntRealEstateValuations){
		List<RealEstateValuations> listRealEstateValuations= new ArrayList<RealEstateValuations>();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			String jsonString = objectMapper.writeValueAsString(listDTOIntRealEstateValuations);
			listRealEstateValuations = objectMapper.readValue(jsonString,new TypeReference<List<RealEstateValuations>>() {});
			
		} catch (JsonProcessingException e) {
			log.error("Error dtoIntRealEstateValuationsToDtoExt Json" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error dtoIntRealEstateValuationsToDtoExt" + e.getMessage());
		}
		return listRealEstateValuations;
	}
	
	public static String dtoIntRealEstateToJsonInfoZona(EntryDataRealEstates entryDataRealEstates) {
		String parameters="";
			
		if((validateNullOrEmpty(entryDataRealEstates.getLatitude())))
				parameters="lat="+entryDataRealEstates.getLatitude();	

		if((validateNullOrEmpty(entryDataRealEstates.getLongitude())))
				parameters+="&long="+entryDataRealEstates.getLongitude();
		
		if((validateNullOrEmpty(entryDataRealEstates.getLimit())))
			parameters+="&limit="+entryDataRealEstates.getLimit();
				
		if((validateNullOrEmpty(entryDataRealEstates.getRadius())))
			parameters+="&kms="+entryDataRealEstates.getRadius();		
		
		if((validateNullOrEmpty(entryDataRealEstates.getRealEstateTypeId())))
			parameters+="&propertyType="+entryDataRealEstates.getRealEstateTypeId().toLowerCase();
	
		if((validateNullOrEmpty(entryDataRealEstates.getRoomsNumber())))
			parameters+="&room="+entryDataRealEstates.getRoomsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstates.getParkingsNumber())))
			parameters+="&parking="+entryDataRealEstates.getParkingsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstates.getBathroomsNumber())))
			parameters+="&bathrooms="+entryDataRealEstates.getBathroomsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstates.getMinimumPrice())))
			parameters+="&precioDesde="+entryDataRealEstates.getMinimumPrice();
		
		if((validateNullOrEmpty(entryDataRealEstates.getMaximumPrice())))
			parameters+="&precioHasta="+entryDataRealEstates.getMaximumPrice();
		
		if((validateNullOrEmpty(entryDataRealEstates.getMinimumSurface())))
			parameters+="&areaDesde="+entryDataRealEstates.getMinimumSurface();

		if((validateNullOrEmpty(entryDataRealEstates.getMaximumSurface())))
			parameters+="&areaHasta="+entryDataRealEstates.getMaximumSurface();
		
		if((validateNullOrEmpty(entryDataRealEstates.getBuildingAge())))
			parameters+="&antiguedad="+entryDataRealEstates.getBuildingAge();
		
		
		if((validateNullOrEmpty(entryDataRealEstates.getDevelopmentTypeId()))){
			String newDevelopments="";
			if(entryDataRealEstates.getDevelopmentTypeId().equalsIgnoreCase("NEW")){
				newDevelopments="1";
			}else if(entryDataRealEstates.getDevelopmentTypeId().equalsIgnoreCase("USED")){
				newDevelopments="0";
			parameters+="&newDevelopments="+newDevelopments;
			}
		}
		return parameters;
}
	
	public static String dtoIntRealEstateToJsonValDir(EntryDataRealEstates entryDataRealEstates) {
		String parameters="";
		
		if((validateNullOrEmpty(entryDataRealEstates.getStateName())))
			parameters+="departamento="+entryDataRealEstates.getStateName().replace(" ", "%20");

		if((validateNullOrEmpty(entryDataRealEstates.getAdministrativeArea3())))
			parameters+="&ciudad="+entryDataRealEstates.getAdministrativeArea3().replace(" ", "%20");

		if((validateNullOrEmpty(entryDataRealEstates.getAddressName())))
			parameters+="&direccion="+entryDataRealEstates.getAddressName().replace(" ", "%20");
				
		if((validateNullOrEmpty(entryDataRealEstates.getAdministrativeArea4())))
			parameters+="&sector="+entryDataRealEstates.getAdministrativeArea4().replace(" ", "%20");		

		return parameters;
	}
	
	public static String parameterRealEstateValuations(EntryDataRealEstateValuations entryDataRealEstateValuations){
		String parameters="";
		if((validateNullOrEmpty(entryDataRealEstateValuations.getLatitude())))
			parameters+="latitude="+entryDataRealEstateValuations.getLatitude();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getLongitude())))
			parameters+="&longitude="+entryDataRealEstateValuations.getLongitude();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getAddressName())))
			parameters+="&direccion="+entryDataRealEstateValuations.getAddressName().replace(" ", "%20");
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getStratumId())))
			parameters+="&estrato="+entryDataRealEstateValuations.getStratumId();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getSurface())))
			parameters+="&m2="+entryDataRealEstateValuations.getSurface();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getRealEstateTypeId()))){	
		String propertyType=entryDataRealEstateValuations.getRealEstateTypeId();
		
			if (entryDataRealEstateValuations.getRealEstateTypeId().equalsIgnoreCase("APARTMENT"))
				propertyType="flat";
		
			parameters+="&propertyType="+propertyType.toLowerCase();
		}		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getBathroomsNumber())))
			parameters+="&bathroom="+entryDataRealEstateValuations.getBathroomsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getParkingsNumber())))
			parameters+="&parking="+entryDataRealEstateValuations.getParkingsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getRoomsNumber())))
			parameters+="&rooms="+entryDataRealEstateValuations.getRoomsNumber();
		
		if((validateNullOrEmpty(entryDataRealEstateValuations.getBuildingAge())))
			parameters+="&rankAntiquity="+entryDataRealEstateValuations.getBuildingAge();
		
		
		 
		return parameters;	
	}

	public static EntryDataRealEstates mapperToEntryDataRealEstates(String addressName,
			String geographicGroupTypeId, String administrativeArea4,
			String stateName, String administrativeArea3, String latitude,
			String longitude, String realEstateTypeId, String parkingsNumber,
			String roomsNumber, String bathroomsNumber, String buildingAge,
			String minimumPrice, String maximumPrice, String minimumSurface,
			String maximumSurface, String radius, String developmentTypeId,
			String limit) {
		EntryDataRealEstates entryDataRealEstates = new EntryDataRealEstates();

		if (validateNullOrEmpty(addressName))
			entryDataRealEstates.setAddressName(addressName);

		if (validateNullOrEmpty(administrativeArea3))
			entryDataRealEstates.setAdministrativeArea3(administrativeArea3);

		if (validateNullOrEmpty(bathroomsNumber))
			entryDataRealEstates.setBathroomsNumber(bathroomsNumber);

		if (validateNullOrEmpty(buildingAge))
			entryDataRealEstates.setBuildingAge(buildingAge);

		if (validateNullOrEmpty(developmentTypeId))
			entryDataRealEstates.setDevelopmentTypeId(developmentTypeId);

		if (validateNullOrEmpty(administrativeArea4))
			entryDataRealEstates.setAdministrativeArea4(administrativeArea4);

		if (validateNullOrEmpty(geographicGroupTypeId))
			entryDataRealEstates.setGeographicGroupTypeId(geographicGroupTypeId);

		if (validateNullOrEmpty(latitude))
			entryDataRealEstates.setLatitude(latitude);

		if (validateNullOrEmpty(longitude))
			entryDataRealEstates.setLongitude(longitude);

		if (validateNullOrEmpty(maximumPrice))
			entryDataRealEstates.setMaximumPrice(maximumPrice);

		if (validateNullOrEmpty(maximumSurface))
			entryDataRealEstates.setMaximumSurface(maximumSurface);

		if (validateNullOrEmpty(minimumPrice))
			entryDataRealEstates.setMinimumPrice(minimumPrice);

		if (validateNullOrEmpty(minimumSurface))
			entryDataRealEstates.setMinimumSurface(minimumSurface);

		if (validateNullOrEmpty(parkingsNumber))
			entryDataRealEstates.setParkingsNumber(parkingsNumber);

		if (validateNullOrEmpty(radius))
			entryDataRealEstates.setRadius(radius);

		if (validateNullOrEmpty(realEstateTypeId))
			entryDataRealEstates.setRealEstateTypeId(realEstateTypeId);

		if (validateNullOrEmpty(bathroomsNumber))
			entryDataRealEstates.setRoomsNumber(bathroomsNumber);

		if (validateNullOrEmpty(stateName))
			entryDataRealEstates.setStateName(stateName);
		
		if (validateNullOrEmpty(limit))
			entryDataRealEstates.setLimit(limit);
		return entryDataRealEstates;
	}
	
	public static EntryDataRealEstateValuations mapperToEntryDataRealEstatesValuations(String radius,
			String stateName, String administrativeArea3,String addressName,String surface,
			String buildingAge, String realEstateTypeId, String roomsNumber, String bathroomsNumber,
			String parkingsNumber, String stratumId, String latitude, String longitude){
		EntryDataRealEstateValuations entryDataRealEstateValuations= new EntryDataRealEstateValuations();
		
		if (validateNullOrEmpty(radius))
			entryDataRealEstateValuations.setRadius(radius);
		
		if (validateNullOrEmpty(stateName))
			entryDataRealEstateValuations.setStateName(stateName);
		
		if (validateNullOrEmpty(administrativeArea3))
			entryDataRealEstateValuations.setAdministrativeArea3(administrativeArea3);
		
		if (validateNullOrEmpty(addressName))
			entryDataRealEstateValuations.setAddressName(addressName);
		
		if (validateNullOrEmpty(surface))
			entryDataRealEstateValuations.setSurface(surface);
		
		if (validateNullOrEmpty(buildingAge))
			entryDataRealEstateValuations.setBuildingAge(buildingAge);
		
		if (validateNullOrEmpty(realEstateTypeId))
			entryDataRealEstateValuations.setRealEstateTypeId(realEstateTypeId);
		
		if (validateNullOrEmpty(roomsNumber))
			entryDataRealEstateValuations.setRoomsNumber(roomsNumber);
		
		if (validateNullOrEmpty(bathroomsNumber))
			entryDataRealEstateValuations.setBathroomsNumber(bathroomsNumber);
		
		if (validateNullOrEmpty(parkingsNumber))
			entryDataRealEstateValuations.setParkingsNumber(parkingsNumber);
		
		if (validateNullOrEmpty(stratumId))
			entryDataRealEstateValuations.setStratumId(stratumId);
		
		if (validateNullOrEmpty(latitude))
			entryDataRealEstateValuations.setLatitude(latitude);
		
		if (validateNullOrEmpty(longitude))
			entryDataRealEstateValuations.setLongitude(longitude);
		
		return entryDataRealEstateValuations;
	}

	public static boolean validateNullOrEmpty(String palabra) {
		return (palabra != null && !palabra.isEmpty()&&!palabra.equalsIgnoreCase("null"));
	}
	public static String validateService(EntryDataRealEstates entryDataRealEstates){
		String service="";
		if(validateNullOrEmpty(entryDataRealEstates.getAdministrativeArea4())&&validateNullOrEmpty(entryDataRealEstates.getAdministrativeArea3()))
			service="validateAddressName";
			return service;
	}

}

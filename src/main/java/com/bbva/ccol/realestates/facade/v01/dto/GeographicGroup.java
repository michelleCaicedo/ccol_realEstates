
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "geographicGroup", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "geographicGroup", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class GeographicGroup
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Code zone where estate is located", required = true)
    private String code;
    @ApiModelProperty(value = "Name zone where estate is located", required = true)
    private String name;
    @ApiModelProperty(value = "Geographic group type", required = true)
    private GeographicGroupTypes geographicGroupTypes;

    public GeographicGroup() {
        //default constructor
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeographicGroupTypes getGeographicGroupTypes() {
        return geographicGroupTypes;
    }

    public void setGeographicGroupTypes(GeographicGroupTypes geographicGroupTypes) {
        this.geographicGroupTypes = geographicGroupTypes;
    }

}

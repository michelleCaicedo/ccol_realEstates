
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "income", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "income", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Income
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Type of income identifier", required = true)
    private IncomeType incomeType;
    @ApiModelProperty(value = "Monthly income value", required = true)
    private IncomeValue incomeValues;

    public Income() {
        //default constructor
    }

    public IncomeType getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(IncomeType incomeType) {
        this.incomeType = incomeType;
    }

    public IncomeValue getIncomeValues() {
        return incomeValues;
    }

    public void setIncomeValues(IncomeValue incomeValues) {
        this.incomeValues = incomeValues;
    }

}


package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "stratum", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "stratum", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Stratum
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Estate stratum identifier", required = true)
    private String id;
    @ApiModelProperty(value = "Description of estate stratum", required = true)
    private String name;

    public Stratum() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}


package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.wordnik.swagger.annotations.ApiModelProperty;



@XmlRootElement(name = "averageAmountsSummaryType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "averageAmountsSummaryType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AverageAmountsSummaryType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Item identifier of summary of average amounts found of properties according to criteria of valuation", required = true)
    private String id;

    public AverageAmountsSummaryType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

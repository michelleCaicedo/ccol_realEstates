
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "amount", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "amount", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Amount
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "ISO 3166-2 with two-digits code geographic state identifier", required = false)
    private Double amount;
    @ApiModelProperty(value = "Name of the geographic state", required = false)
    private String currency;

    public Amount() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}

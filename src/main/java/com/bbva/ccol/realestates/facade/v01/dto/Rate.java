
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "rate", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "rate", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rate
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Id of estate type", required = true)
    private RateType rateType;
    @ApiModelProperty(value = "Mode in which the collection rate is applied", required = false)
    private Mode mode;
    @ApiModelProperty(value = "Mode in which the collection rate is applied", required = true)
    private Percentage unit;

    public Rate() {
        //default constructor
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Percentage getUnit() {
        return unit;
    }

    public void setUnit(Percentage unit) {
        this.unit = unit;
    }

}

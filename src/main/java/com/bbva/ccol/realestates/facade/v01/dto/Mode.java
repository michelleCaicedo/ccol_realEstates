
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "mode", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "mode", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Mode
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier of the mode that applies the collection of the rate", required = false)
    private String id;
    @ApiModelProperty(value = "Name of the mode that applies the collection of the rate", required = false)
    private IncomeValue name;

    public Mode() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IncomeValue getName() {
        return name;
    }

    public void setName(IncomeValue name) {
        this.name = name;
    }

}

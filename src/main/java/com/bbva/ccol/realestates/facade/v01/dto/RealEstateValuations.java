
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.ccol.realestates.business.dto.DTOIntPriceSnapshot;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "realEstateValuations", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "realEstateValuations", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RealEstateValuations
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Estate type", required = false)
    private RealEstateType realEstateType;
    @ApiModelProperty(value = "Estate location", required = false)
    private Location location;
    @ApiModelProperty(value = "The value of the estimated area on which the calculation of the sale price has been made will be displayed", required = true)
    private Double radius;
    @ApiModelProperty(value = "Degree of accuracy with which the calculation has been made", required = true)
    private String precision;
    @ApiModelProperty(value = "Estate description", required = false)
    private RealEstatesSummaryItem foundRealEstatesSummary;
    @ApiModelProperty(value = "Value of the estimated sale price", required = false)
    private Amount price;
    @ApiModelProperty(value = "Evolution of the estimated price on sale", required = false)
    private List<PriceSnapshot> priceSnapshots;
    @ApiModelProperty(value = "Summary of information of averages found of properties according to criteria of valuation", required = false)
    private List<AverageAmountsSummaryItem> averageAmountsSummary;
    @ApiModelProperty(value = "Financing information related to the estate", required = false)
    private FinancingData financingData;
    @ApiModelProperty(value = "Real estates relationed with the valuation results", required = false)
    private List<RealEstates> listRealEstates;

    public RealEstateValuations() {
        //default constructor
    }

    public RealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(RealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public RealEstatesSummaryItem getFoundRealEstatesSummary() {
        return foundRealEstatesSummary;
    }

    public void setFoundRealEstatesSummary(RealEstatesSummaryItem foundRealEstatesSummary) {
        this.foundRealEstatesSummary = foundRealEstatesSummary;
    }

    public Amount getPrice() {
        return price;
    }

    public void setPrice(Amount price) {
        this.price = price;
    }

    public FinancingData getFinancingData() {
        return financingData;
    }

    public void setFinancingData(FinancingData financingData) {
        this.financingData = financingData;
    }

	public List<RealEstates> getListRealEstates() {
		return listRealEstates;
	}

	public void setListRealEstates(List<RealEstates> listRealEstates) {
		this.listRealEstates = listRealEstates;
	}

	public List<PriceSnapshot> getPriceSnapshots() {
		return priceSnapshots;
	}

	public void setPriceSnapshots(List<PriceSnapshot> priceSnapshots) {
		this.priceSnapshots = priceSnapshots;
	}

	public List<AverageAmountsSummaryItem> getAverageAmountsSummary() {
		return averageAmountsSummary;
	}

	public void setAverageAmountsSummary(
			List<AverageAmountsSummaryItem> averageAmountsSummary) {
		this.averageAmountsSummary = averageAmountsSummary;
	}



}

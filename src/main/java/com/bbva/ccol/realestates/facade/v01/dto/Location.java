
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "location", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "location", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Estate address", required = false)
    private String addressName;
    @ApiModelProperty(value = "State information where the estate is located", required = true)
    private State state;
    @ApiModelProperty(value = "Geographic information", required = true)
    private GeographicGroup geographicGroup;
    @ApiModelProperty(value = "State information where the estate is located", required = false)
    private String zipCode;

    public Location() {
        //default constructor
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public GeographicGroup getGeographicGroup() {
        return geographicGroup;
    }

    public void setGeographicGroup(GeographicGroup geographicGroup) {
        this.geographicGroup = geographicGroup;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}

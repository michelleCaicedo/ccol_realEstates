package com.bbva.ccol.realestates.facade.v01;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.ccol.realestates.business.ISrvIntRealEstates;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstates;
import com.bbva.ccol.realestates.facade.v01.dto.RealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.RealEstates;
import com.bbva.ccol.realestates.facade.v01.mapper.Mapper;
import com.bbva.ccol.realestates.properties.RealEstatesProperties;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;


	
@Path("/V01")
@SN(registryID="SNCO1810006",logicalID="realEstates")
@VN(vnn="V01")
@Api(value="/realEstates/V01",description="This API includes any service related to the transactions of the custormes with products of another entitys")
@Produces({ MediaType.APPLICATION_JSON})
@Service

	
public class SrvRealEstatesV01 implements ISrvRealEstatesV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static I18nLog log = I18nLogFactory.getLogI18n(SrvRealEstatesV01.class,"META-INF/spring/i18n/log/mensajesLog");

	public HttpHeaders httpHeaders;
	
	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	
	@Resource(name = "realEstatesProperties")
	private RealEstatesProperties realEstatesProperties;
	
	public UriInfo uriInfo;
	
	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;		
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}
	
	@Autowired
	ISrvIntRealEstates srvIntRealEstates;
	/**
	* Proyecto:Madiva Vivienda Online
	* Autor: Michelle Leonor Caicedo Espinel - michelleleonor.caicedo@bbva.com
	* Version: V01
	* Base URL: https://bbva-apicatalog.appspot.com/#/global/apis/data/realEstates
	* Git Repository: https://globaldevtools.bbva.com/bitbucket/projects/glapi/repos/glapi-global-apis-data-realestates
	**/	
	@ApiOperation(value="The zone report service includes the possibility of showing users the offer of available properties in a specific area", notes="",response=Response.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Found Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@GET
	@Path("/realEstates")
	@SMC(registryID="SMCCO1810012",logicalID="getRealEstates")
	public Response getRealEstates(@ApiParam(value = "addressName") @DefaultValue("null") @QueryParam("addressName") String addressName,
			@ApiParam(value = "geographicGroupType.id") @DefaultValue("null") @QueryParam("geographicGroupType.id") String geographicGroupTypeId,
			@ApiParam(value = "administrativeArea4") @DefaultValue("null") @QueryParam("administrativeArea4") String administrativeArea4,
			@ApiParam(value = "state.name") @DefaultValue("null") @QueryParam("state.name") String stateName,
			@ApiParam(value = "administrativeArea3") @DefaultValue("null") @QueryParam("administrativeArea3") String administrativeArea3,
			@ApiParam(value = "latitude") @DefaultValue("null") @QueryParam("latitude") String latitude,
			@ApiParam(value = "longitude") @DefaultValue("null") @QueryParam("longitude") String longitude,
			@ApiParam(value = "realEstateType.id") @DefaultValue("null") @QueryParam("realEstateType.id") String realEstateTypeId,
			@ApiParam(value = "parkingsNumber") @DefaultValue("null") @QueryParam("parkingsNumber") String parkingsNumber,
			@ApiParam(value = "roomsNumber") @DefaultValue("null") @QueryParam("roomsNumber") String roomsNumber,
			@ApiParam(value = "bathroomsNumber") @DefaultValue("null") @QueryParam("bathroomsNumber") String bathroomsNumber,
			@ApiParam(value = "buildingAge") @DefaultValue("null") @QueryParam("buildingAge") String buildingAge,
			@ApiParam(value = "minimumPrice") @DefaultValue("null") @QueryParam("minimumPrice") String minimumPrice,
			@ApiParam(value = "maximumPrice") @DefaultValue("null") @QueryParam("maximumPrice") String maximumPrice,
			@ApiParam(value = "minimumSurface") @DefaultValue("null") @QueryParam("minimumSurface") String minimumSurface,
			@ApiParam(value = "maximumSurface") @DefaultValue("null") @QueryParam("maximumSurface") String maximumSurface,
			@ApiParam(value = "radius") @DefaultValue("null") @QueryParam("radius") String radius,
			@ApiParam(value = "developmentType.id") @DefaultValue("null") @QueryParam("developmentType.id") String developmentTypeId) {
		
		EntryDataRealEstates entryDataRealEstates= new EntryDataRealEstates();
		String limit=realEstatesProperties.getProperty("madiva.limit");
		entryDataRealEstates = Mapper.mapperToEntryDataRealEstates(addressName, geographicGroupTypeId, administrativeArea4, stateName, administrativeArea3,  latitude,  longitude,  realEstateTypeId,  parkingsNumber, roomsNumber, bathroomsNumber,  buildingAge,  minimumPrice,  maximumPrice,  minimumSurface,  maximumSurface,  radius,  developmentTypeId, limit);
		List<RealEstates>  data= new  ArrayList<RealEstates>();
		data=Mapper.dtoIntRealEstatesToDtoExt(srvIntRealEstates.getRealEstates(entryDataRealEstates));
		ResponseBuilder rb = null;
		if(data!=null){
			rb= Response.ok(data);
			rb.header("googleKey",realEstatesProperties.getProperty("google.api.key"));	
			rb.header("MadivaConsumer",realEstatesProperties.getProperty("parameter.madiva"));
		}
		return rb.build();
	}
	
	/**
	* Proyecto:Madiva Vivienda Online
	* Autor: Michelle Leonor Caicedo Espinel - michelleleonor.caicedo@bbva.com
	* Version: V01
	* Base URL: https://bbva-apicatalog.appspot.com/#/global/apis/data/realEstates
	* Git Repository: https://globaldevtools.bbva.com/bitbucket/projects/glapi/repos/glapi-global-apis-data-realestates
	**/		
	@ApiOperation(value="It allows to determine the value of a house in a certain direction under some selection criteria that the user makes It also allows to know a negotiation value, the possible value of rent and relevant information in order to facilitate the decision making of buying selling a property", notes="",response=Response.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Found Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@GET
	@Path("/realEstatesValuations")
	@SMC(registryID="SMCCO1810013",logicalID="getRealEstatesValuations")
	public Response getRealEstateValuations(@ApiParam(value = "radius") @DefaultValue("null") @QueryParam("radius") String radius,
			@ApiParam(value = "state.name") @DefaultValue("null") @QueryParam("state.name") String stateName,
			@ApiParam(value = "administrativeArea3") @DefaultValue("null") @QueryParam("administrativeArea3") String administrativeArea3,
			@ApiParam(value = "addressName") @DefaultValue("null") @QueryParam("addressName") String addressName,
			@ApiParam(value = "surface") @DefaultValue("null") @QueryParam("surface") String surface,
			@ApiParam(value = "buildingAge") @DefaultValue("null") @QueryParam("buildingAge") String buildingAge,
			@ApiParam(value = "realEstateType.id") @DefaultValue("null") @QueryParam("realEstateType.id") String realEstateTypeId,
			@ApiParam(value = "roomsNumber") @DefaultValue("null") @QueryParam("roomsNumber") String roomsNumber,
			@ApiParam(value = "bathroomsNumber") @DefaultValue("null") @QueryParam("bathroomsNumber") String bathroomsNumber,
			@ApiParam(value = "parkingsNumber") @DefaultValue("null") @QueryParam("parkingsNumber") String parkingsNumber,
			@ApiParam(value = "stratum.id") @DefaultValue("null") @QueryParam("stratum.id") String stratumId,
			@ApiParam(value = "latitude") @DefaultValue("null") @QueryParam("latitude") String latitude,
			@ApiParam(value = "longitude") @DefaultValue("null") @QueryParam("longitude") String longitude) {
		
		EntryDataRealEstateValuations entryDataRealEstateValuations = new EntryDataRealEstateValuations();
		entryDataRealEstateValuations =Mapper.mapperToEntryDataRealEstatesValuations(radius,  stateName,  administrativeArea3, addressName, surface,  buildingAge,  realEstateTypeId,  roomsNumber,  bathroomsNumber,  parkingsNumber,  stratumId,  latitude,  longitude);
		List<RealEstateValuations> data=new  ArrayList<RealEstateValuations>();
		data=Mapper.dtoIntRealEstateValuationsToDtoExt(srvIntRealEstates.getRealEstateValuations(entryDataRealEstateValuations));
		ResponseBuilder rb = null;		
		if(data!=null){
			rb= Response.ok(data);
			rb.header("googleKey",realEstatesProperties.getProperty("google.api.key"));	
			rb.header("MadivaConsumer",realEstatesProperties.getProperty("parameter.madiva"));
		}
		return rb.build();
	}

	

}

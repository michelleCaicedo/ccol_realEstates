package com.bbva.ccol.realestates.facade.v01.dto;

public class EntryDataRealEstateValuations {
	
	private String radius;
	private String stateName;
	private String administrativeArea3;
	private String addressName;
	private String surface;
	private String buildingAge;
	private String realEstateTypeId;
	private String roomsNumber;
	private String bathroomsNumber;
	private String parkingsNumber;
	private String stratumId;
	private String latitude;
	private String longitude;
	
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getAdministrativeArea3() {
		return administrativeArea3;
	}
	public void setAdministrativeArea3(String administrativeArea3) {
		this.administrativeArea3 = administrativeArea3;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getSurface() {
		return surface;
	}
	public void setSurface(String surface) {
		this.surface = surface;
	}
	public String getBuildingAge() {
		return buildingAge;
	}
	public void setBuildingAge(String buildingAge) {
		this.buildingAge = buildingAge;
	}
	public String getRealEstateTypeId() {
		return realEstateTypeId;
	}
	public void setRealEstateTypeId(String realEstateTypeId) {
		this.realEstateTypeId = realEstateTypeId;
	}
	public String getRoomsNumber() {
		return roomsNumber;
	}
	public void setRoomsNumber(String roomsNumber) {
		this.roomsNumber = roomsNumber;
	}
	public String getBathroomsNumber() {
		return bathroomsNumber;
	}
	public void setBathroomsNumber(String bathroomsNumber) {
		this.bathroomsNumber = bathroomsNumber;
	}
	public String getParkingsNumber() {
		return parkingsNumber;
	}
	public void setParkingsNumber(String parkingsNumber) {
		this.parkingsNumber = parkingsNumber;
	}
	public String getStratumId() {
		return stratumId;
	}
	public void setStratumId(String stratumId) {
		this.stratumId = stratumId;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}

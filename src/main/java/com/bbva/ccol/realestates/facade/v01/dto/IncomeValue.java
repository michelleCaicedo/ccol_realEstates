
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "incomeValue", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "incomeValue", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class IncomeValue
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Monetary amount", required = false)
    private Long amount;
    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the amount", required = false)
    private String currency;
    @ApiModelProperty(value = "Frequency of income", required = true)
    private String frequency;

    public IncomeValue() {
        //default constructor
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

}

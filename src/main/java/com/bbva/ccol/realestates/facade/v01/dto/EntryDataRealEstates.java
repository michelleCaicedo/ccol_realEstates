package com.bbva.ccol.realestates.facade.v01.dto;

public class EntryDataRealEstates {
	private String addressName;
	private String geographicGroupTypeId;
	private String administrativeArea4;
	private String stateName;
	private String administrativeArea3;
	private String latitude;
	private String longitude;
	private String realEstateTypeId;
	private String parkingsNumber;
	private String roomsNumber;
	private String bathroomsNumber;
	private String buildingAge;
	private String minimumPrice;
	private String maximumPrice;
	private String minimumSurface;
	private String maximumSurface;
	private String radius;
	private String developmentTypeId;
	private String limit;
	
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getGeographicGroupTypeId() {
		return geographicGroupTypeId;
	}
	public void setGeographicGroupTypeId(String geographicGroupTypeId) {
		this.geographicGroupTypeId = geographicGroupTypeId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getAdministrativeArea3() {
		return administrativeArea3;
	}
	public void setAdministrativeArea3(String administrativeArea3) {
		this.administrativeArea3 = administrativeArea3;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getRealEstateTypeId() {
		return realEstateTypeId;
	}
	public void setRealEstateTypeId(String realEstateTypeId) {
		this.realEstateTypeId = realEstateTypeId;
	}
	public String getParkingsNumber() {
		return parkingsNumber;
	}
	public void setParkingsNumber(String parkingsNumber) {
		this.parkingsNumber = parkingsNumber;
	}
	public String getRoomsNumber() {
		return roomsNumber;
	}
	public void setRoomsNumber(String roomsNumber) {
		this.roomsNumber = roomsNumber;
	}
	public String getBathroomsNumber() {
		return bathroomsNumber;
	}
	public void setBathroomsNumber(String bathroomsNumber) {
		this.bathroomsNumber = bathroomsNumber;
	}
	public String getBuildingAge() {
		return buildingAge;
	}
	public void setBuildingAge(String buildingAge) {
		this.buildingAge = buildingAge;
	}
	public String getMinimumPrice() {
		return minimumPrice;
	}
	public void setMinimumPrice(String minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
	public String getMaximumPrice() {
		return maximumPrice;
	}
	public void setMaximumPrice(String maximumPrice) {
		this.maximumPrice = maximumPrice;
	}
	public String getMinimumSurface() {
		return minimumSurface;
	}
	public void setMinimumSurface(String minimumSurface) {
		this.minimumSurface = minimumSurface;
	}
	public String getMaximumSurface() {
		return maximumSurface;
	}
	public void setMaximumSurface(String maximumSurface) {
		this.maximumSurface = maximumSurface;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getDevelopmentTypeId() {
		return developmentTypeId;
	}
	public void setDevelopmentTypeId(String developmentTypeId) {
		this.developmentTypeId = developmentTypeId;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getAdministrativeArea4() {
		return administrativeArea4;
	}
	public void setAdministrativeArea4(String administrativeArea4) {
		this.administrativeArea4 = administrativeArea4;
	}
}

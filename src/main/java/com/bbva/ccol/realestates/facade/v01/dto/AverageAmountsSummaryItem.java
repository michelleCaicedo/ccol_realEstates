
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "averageAmountsSummaryItem", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "averageAmountsSummaryItem", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AverageAmountsSummaryItem
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Purpose type identifier", required = true)
    private AverageAmountsSummaryType averageAmountsSummaryType;
    @ApiModelProperty(value = "Value of the average amount according to the type", required = false)
    private Amount averageAmount;

    public AverageAmountsSummaryItem() {
        //default constructor
    }

    public AverageAmountsSummaryType getAverageAmountsSummaryType() {
        return averageAmountsSummaryType;
    }

    public void setAverageAmountsSummaryType(AverageAmountsSummaryType averageAmountsSummaryType) {
        this.averageAmountsSummaryType = averageAmountsSummaryType;
    }

    public Amount getAverageAmount() {
        return averageAmount;
    }

    public void setAverageAmount(Amount averageAmount) {
        this.averageAmount = averageAmount;
    }

}

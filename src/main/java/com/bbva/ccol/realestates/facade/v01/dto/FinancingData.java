
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "financingData", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "financingData", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancingData
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "The value of the home X 80 Value to be financed in mortgage or VP (Valor presente, spanish for Present Value) of the amount to be financed", required = true)
    private Amount financingAmount;
    @ApiModelProperty(value = "Average placement term in Colombia, 15 years for all cases", required = true)
    private Term terms;
    @ApiModelProperty(value = "It contains the data of the interest rates", required = true)
    private Rate rates;
    @ApiModelProperty(value = "Name of estate type", required = true)
    private Amount installmentAmount;
    @ApiModelProperty(value = "Name of estate type", required = true)
    private Income estimatedIncome;

    public FinancingData() {
        //default constructor
    }

    public Amount getFinancingAmount() {
        return financingAmount;
    }

    public void setFinancingAmount(Amount financingAmount) {
        this.financingAmount = financingAmount;
    }

    public Term getTerms() {
        return terms;
    }

    public void setTerms(Term terms) {
        this.terms = terms;
    }

    public Rate getRates() {
        return rates;
    }

    public void setRates(Rate rates) {
        this.rates = rates;
    }

    public Amount getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(Amount installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public Income getEstimatedIncome() {
        return estimatedIncome;
    }

    public void setEstimatedIncome(Income estimatedIncome) {
        this.estimatedIncome = estimatedIncome;
    }

}

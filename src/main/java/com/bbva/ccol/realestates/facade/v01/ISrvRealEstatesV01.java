package com.bbva.ccol.realestates.facade.v01;

import javax.ws.rs.core.Response;


public interface ISrvRealEstatesV01 {
 	public Response getRealEstates(String addressName,String geographicGroupTypeId,String administrativeArea4,String stateName,String administrativeArea3, String latitude, String longitude, String realEstateTypeId, String parkingsNumber,String roomsNumber,String bathroomsNumber, String buildingAge, String minimumPrice, String maximumPrice, String minimumSurface, String maximumSurface, String radius, String developmentTypeId);

	public Response getRealEstateValuations(String radius, String stateName, String administrativeArea3,String addressName,String surface, String buildingAge, String realEstateTypeId, String roomsNumber, String bathroomsNumber, String parkingsNumber, String stratumId, String latitude, String longitude);

	
}
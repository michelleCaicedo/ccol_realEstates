
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.wordnik.swagger.annotations.ApiModelProperty;



@XmlRootElement(name = "percentage", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "percentage", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Percentage
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Rate type identifier", required = true)
    private String percentage;

    public Percentage() {
        //default constructor
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

}

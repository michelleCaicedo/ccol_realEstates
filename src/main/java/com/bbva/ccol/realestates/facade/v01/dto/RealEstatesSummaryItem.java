
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "realEstatesSummaryItem", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "realEstatesSummaryItem", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RealEstatesSummaryItem
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Item type with information summary about estates", required = true)
    private RealEstatesSummaryItemType realEstatesSummaryItemType;
    @ApiModelProperty(value = "Value of the item according to its type", required = true)
    private Long value;

    public RealEstatesSummaryItem() {
        //default constructor
    }

    public RealEstatesSummaryItemType getRealEstatesSummaryItemType() {
        return realEstatesSummaryItemType;
    }

    public void setRealEstatesSummaryItemType(RealEstatesSummaryItemType realEstatesSummaryItemType) {
        this.realEstatesSummaryItemType = realEstatesSummaryItemType;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}

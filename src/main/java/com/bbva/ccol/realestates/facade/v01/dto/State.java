
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "state", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "state", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class State
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "State, Province or Region where the estate is placed", required = true)
    private String id;
    @ApiModelProperty(value = "Name state, province or region", required = true)
    private String name;

    public State() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

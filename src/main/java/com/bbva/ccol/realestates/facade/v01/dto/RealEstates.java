
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "realEstates", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "realEstates", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RealEstates
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier of the estate", required = true)
    private String id;
    @ApiModelProperty(value = "Estate description", required = false)
    private String description;
    @ApiModelProperty(value = "Estate location", required = false)
    private Location location;
    @ApiModelProperty(value = "Surface in square meters of the estate", required = false)
    private Double surface;
    @ApiModelProperty(value = "Image URL", required = false)
    private String imageUrl;
    @ApiModelProperty(value = "Estate description", required = false)
    private String propertyInformationWeb;
    @ApiModelProperty(value = "Estate type", required = false)
    private RealEstateType realEstateType;
    @ApiModelProperty(value = "Estate status", required = false)
    private DevelopmentType developmentType;
    @ApiModelProperty(value = "Estate price", required = false)
    private Amount price;
    @ApiModelProperty(value = "Estate price by meter", required = false)
    private Amount priceByMeter;
    @ApiModelProperty(value = "Parkings number", required = false)
    private Long parkingsNumber;
    @ApiModelProperty(value = " Rooms number", required = false)
    private Long roomsNumber;
    @ApiModelProperty(value = "Estate type", required = false)
    private Long bathroomsNumber;
    @ApiModelProperty(value = "Years of antique that has the estate", required = false)
    private Long buildingAge;
    @ApiModelProperty(value = "Level or grade of real estate location", required = false)
    private Stratum stratum;
    @ApiModelProperty(value = "Classification of the use of the estate", required = false)
    private PurposeType purposeType;
    @ApiModelProperty(value = " Average surface area in square meters", required = true)
    private Double averageSurface;

    public RealEstates() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getSurface() {
        return surface;
    }

    public void setSurface(Double surface) {
        this.surface = surface;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPropertyInformationWeb() {
        return propertyInformationWeb;
    }

    public void setPropertyInformationWeb(String propertyInformationWeb) {
        this.propertyInformationWeb = propertyInformationWeb;
    }

    public RealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(RealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public DevelopmentType getDevelopmentType() {
        return developmentType;
    }

    public void setDevelopmentType(DevelopmentType developmentType) {
        this.developmentType = developmentType;
    }

    public Amount getPrice() {
        return price;
    }

    public void setPrice(Amount price) {
        this.price = price;
    }

    public Amount getPriceByMeter() {
        return priceByMeter;
    }

    public void setPriceByMeter(Amount priceByMeter) {
        this.priceByMeter = priceByMeter;
    }

    public Long getParkingsNumber() {
        return parkingsNumber;
    }

    public void setParkingsNumber(Long parkingsNumber) {
        this.parkingsNumber = parkingsNumber;
    }

    public Long getRoomsNumber() {
        return roomsNumber;
    }

    public void setRoomsNumber(Long roomsNumber) {
        this.roomsNumber = roomsNumber;
    }

    public Long getBathroomsNumber() {
        return bathroomsNumber;
    }

    public void setBathroomsNumber(Long bathroomsNumber) {
        this.bathroomsNumber = bathroomsNumber;
    }

    public Long getBuildingAge() {
        return buildingAge;
    }

    public void setBuildingAge(Long buildingAge) {
        this.buildingAge = buildingAge;
    }

    public Stratum getStratum() {
        return stratum;
    }

    public void setStratum(Stratum stratum) {
        this.stratum = stratum;
    }

    public PurposeType getPurposeType() {
        return purposeType;
    }

    public void setPurposeType(PurposeType purposeType) {
        this.purposeType = purposeType;
    }

    public Double getAverageSurface() {
        return averageSurface;
    }

    public void setAverageSurface(Double averageSurface) {
        this.averageSurface = averageSurface;
    }

}

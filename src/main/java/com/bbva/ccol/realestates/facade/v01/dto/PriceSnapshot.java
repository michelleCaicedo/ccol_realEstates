
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "priceSnapshot", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "priceSnapshot", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PriceSnapshot
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Purpose type identifier", required = false)
    private Amount price;
    @ApiModelProperty(value = "Last 12 months from the consultation", required = false)
    private Date month;
    @ApiModelProperty(value = "Percentage value of the evolution per month", required = false)
    private Long percentage;

    public PriceSnapshot() {
        //default constructor
    }

    public Amount getPrice() {
        return price;
    }

    public void setPrice(Amount price) {
        this.price = price;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public Long getPercentage() {
        return percentage;
    }

    public void setPercentage(Long percentage) {
        this.percentage = percentage;
    }

}

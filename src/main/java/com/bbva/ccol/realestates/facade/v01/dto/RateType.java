
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "rateType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "rateType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Rate type identifier", required = true)
    private String id;
    @ApiModelProperty(value = "Name of rate type", required = false)
    private String name;

    public RateType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

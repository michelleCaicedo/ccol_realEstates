
package com.bbva.ccol.realestates.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "purposeType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlType(name = "purposeType", namespace = "urn:com:bbva:ccol:realestates:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PurposeType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Purpose type identifier", required = false)
    private String id;
    @ApiModelProperty(value = "Purpose type name", required = false)
    private String name;

    public PurposeType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

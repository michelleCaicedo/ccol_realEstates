
package com.bbva.ccol.realestates.business.dto;




public class DTOIntPercentage {

    public final static long serialVersionUID = 1L;
    private String percentage;

    public DTOIntPercentage() {
        //default constructor
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

}

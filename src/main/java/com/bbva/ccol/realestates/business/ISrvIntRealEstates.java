package com.bbva.ccol.realestates.business;

import java.util.List;

import com.bbva.ccol.realestates.business.dto.DTOIntRealEstateValuations;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstates;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstates;



public interface ISrvIntRealEstates {
 	public List<DTOIntRealEstates> getRealEstates(EntryDataRealEstates entryDataRealEstates);

		public List<DTOIntRealEstateValuations> getRealEstateValuations(EntryDataRealEstateValuations entryDataRealEstateValuations);

	
}
package com.bbva.ccol.realestates.business.dto;

public class comparable {
	private String id;
	private String description;
	private Double area_m2;
	private Double price;
	private Long bathroom;
	private String longitude;
	private String latitude;
	private String linkportal;
	private String address;
	private Double price_m2;
	private Long parking;
	private Long rooms;
	private String img_url;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getArea_m2() {
		return area_m2;
	}
	public void setArea_m2(Double area_m2) {
		this.area_m2 = area_m2;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getBathroom() {
		return bathroom;
	}
	public void setBathroom(Long bathroom) {
		this.bathroom = bathroom;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLinkportal() {
		return linkportal;
	}
	public void setLinkportal(String linkportal) {
		this.linkportal = linkportal;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getPrice_m2() {
		return price_m2;
	}
	public void setPrice_m2(Double price_m2) {
		this.price_m2 = price_m2;
	}
	public Long getParking() {
		return parking;
	}
	public void setParking(Long parking) {
		this.parking = parking;
	}
	public Long getRooms() {
		return rooms;
	}
	public void setRooms(Long rooms) {
		this.rooms = rooms;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	}

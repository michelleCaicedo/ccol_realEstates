
package com.bbva.ccol.realestates.business.dto;




public class DTOIntGeographicGroup {

    public final static long serialVersionUID = 1L;
    private String code;
    private String name;
    private DTOIntGeographicGroupTypes geographicGroupTypes;

    public DTOIntGeographicGroup() {
        //default constructor
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntGeographicGroupTypes getGeographicGroupTypes() {
        return geographicGroupTypes;
    }

    public void setGeographicGroupTypes(DTOIntGeographicGroupTypes geographicGroupTypes) {
        this.geographicGroupTypes = geographicGroupTypes;
    }

}

package com.bbva.ccol.realestates.business.dto;



public class ValuationsMadiva {

	private Double cuota_hipoteca;
	private Double ingresos_minimos;
	private Double avgmetros;
	private Double precioventa;
	private Double avgpreciom2;
	private Double longitud;
	private Double avgprecio;
	private String precision;
	private String latitud;
	private Double precio_arriendo;
	private Double precio_arriendo_m2;
	private historico_precio historico_precio;

	
	public Double getAvgmetros() {
		return avgmetros;
	}
	public void setAvgmetros(Double avgmetros) {
		this.avgmetros = avgmetros;
	}
	public Double getAvgprecio() {
		return avgprecio;
	}
	public void setAvgprecio(Double avgprecio) {
		this.avgprecio = avgprecio;
	}
	
	public String getPrecision() {
		return precision;
	}
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	public Double getCuota_hipoteca() {
		return cuota_hipoteca;
	}
	public void setCuota_hipoteca(Double cuota_hipoteca) {
		this.cuota_hipoteca = cuota_hipoteca;
	}
	public Double getIngresos_minimos() {
		return ingresos_minimos;
	}
	public void setIngresos_minimos(Double ingresos_minimos) {
		this.ingresos_minimos = ingresos_minimos;
	}
	public Double getPrecioventa() {
		return precioventa;
	}
	public void setPrecioventa(Double precioventa) {
		this.precioventa = precioventa;
	}
	public Double getAvgpreciom2() {
		return avgpreciom2;
	}
	public void setAvgpreciom2(Double avgpreciom2) {
		this.avgpreciom2 = avgpreciom2;
	}
	public Double getLongitud() {
		return longitud;
	}
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public Double getPrecio_arriendo() {
		return precio_arriendo;
	}
	public void setPrecio_arriendo(Double precio_arriendo) {
		this.precio_arriendo = precio_arriendo;
	}
	public Double getPrecio_arriendo_m2() {
		return precio_arriendo_m2;
	}
	public void setPrecio_arriendo_m2(Double precio_arriendo_m2) {
		this.precio_arriendo_m2 = precio_arriendo_m2;
	}
	public historico_precio getHistorico_precio() {
		return historico_precio;
	}
	public void setHistorico_precio(historico_precio historico_precio) {
		this.historico_precio = historico_precio;
	}
}

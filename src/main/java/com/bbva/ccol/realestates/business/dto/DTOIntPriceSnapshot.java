
package com.bbva.ccol.realestates.business.dto;

import java.util.Date;



public class DTOIntPriceSnapshot {

    public final static long serialVersionUID = 1L;
    private DTOIntAmount price;
    private Date month;
    private Double percentage;

    public DTOIntPriceSnapshot() {
        //default constructor
    }

    public DTOIntAmount getPrice() {
        return price;
    }

    public void setPrice(DTOIntAmount price) {
        this.price = price;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

}

package com.bbva.ccol.realestates.business.dto;

public class by_room {
	private String room;
	private Double surface;
	private Double pricem2;
	private Double price;
	
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public Double getSurface() {
		return surface;
	}
	public void setSurface(Double surface) {
		this.surface = surface;
	}
	public Double getPricem2() {
		return pricem2;
	}
	public void setPricem2(Double pricem2) {
		this.pricem2 = pricem2;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}

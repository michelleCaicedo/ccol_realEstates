package com.bbva.ccol.realestates.business.dto;

public class precios {
	private String year;
	private String mes;
	private Double precio_m2;
	private String comparables;
	private Double avgmeters;
	private Double avgprice;
	private Double stdevmeters;
	private Double stdevprice;
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Double getPrecio_m2() {
		return precio_m2;
	}
	public void setPrecio_m2(Double precio_m2) {
		this.precio_m2 = precio_m2;
	}
	public String getComparables() {
		return comparables;
	}
	public void setComparables(String comparables) {
		this.comparables = comparables;
	}
	public Double getAvgmeters() {
		return avgmeters;
	}
	public void setAvgmeters(Double avgmeters) {
		this.avgmeters = avgmeters;
	}
	public Double getAvgprice() {
		return avgprice;
	}
	public void setAvgprice(Double avgprice) {
		this.avgprice = avgprice;
	}
	public Double getStdevmeters() {
		return stdevmeters;
	}
	public void setStdevmeters(Double stdevmeters) {
		this.stdevmeters = stdevmeters;
	}
	public Double getStdevprice() {
		return stdevprice;
	}
	public void setStdevprice(Double stdevprice) {
		this.stdevprice = stdevprice;
	}

}

package com.bbva.ccol.realestates.business.dto;

import java.util.List;

public class ZoneInfoMadiva {
	
	private List<house_list> house_list;
	private List<department_list> department_list;
	
	public List<house_list> getHouse_list() {
		return house_list;
	}
	public void setHouse_list(List<house_list> house_list) {
		this.house_list = house_list;
	}
	public List<department_list> getDepartment_list() {
		return department_list;
	}
	public void setDepartment_list(List<department_list> department_list) {
		this.department_list = department_list;
	}
	
	

	
}


package com.bbva.ccol.realestates.business.dto;

import java.util.List;


public class DTOIntRealEstateValuations {

    public final static long serialVersionUID = 1L;
    private DTOIntRealEstateType realEstateType;
    private DTOIntLocation location;
    private Double radius;
    private String precision;
    private DTOIntRealEstatesSummaryItem foundRealEstatesSummary;
    private DTOIntAmount price;
    private List<DTOIntPriceSnapshot> priceSnapshots;
    private List<DTOIntAverageAmountsSummaryItem> averageAmountsSummary;
    private DTOIntFinancingData financingData;
    private List<DTOIntRealEstates> listRealEstates;

    public DTOIntRealEstateValuations() {
        //default constructor
    }

    public DTOIntRealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(DTOIntRealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public DTOIntLocation getLocation() {
        return location;
    }

    public void setLocation(DTOIntLocation location) {
        this.location = location;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public DTOIntRealEstatesSummaryItem getFoundRealEstatesSummary() {
        return foundRealEstatesSummary;
    }

    public void setFoundRealEstatesSummary(DTOIntRealEstatesSummaryItem foundRealEstatesSummary) {
        this.foundRealEstatesSummary = foundRealEstatesSummary;
    }

    public DTOIntAmount getPrice() {
        return price;
    }

    public void setPrice(DTOIntAmount price) {
        this.price = price;
    }


    public DTOIntFinancingData getFinancingData() {
        return financingData;
    }

    public void setFinancingData(DTOIntFinancingData financingData) {
        this.financingData = financingData;
    }

	public List<DTOIntRealEstates> getListRealEstates() {
		return listRealEstates;
	}

	public void setListRealEstates(List<DTOIntRealEstates> listRealEstates) {
		this.listRealEstates = listRealEstates;
	}

	public List<DTOIntAverageAmountsSummaryItem> getAverageAmountsSummary() {
		return averageAmountsSummary;
	}

	public void setAverageAmountsSummary(
			List<DTOIntAverageAmountsSummaryItem> averageAmountsSummary) {
		this.averageAmountsSummary = averageAmountsSummary;
	}

	public List<DTOIntPriceSnapshot> getPriceSnapshots() {
		return priceSnapshots;
	}

	public void setPriceSnapshots(List<DTOIntPriceSnapshot> priceSnapshots) {
		this.priceSnapshots = priceSnapshots;
	}


}

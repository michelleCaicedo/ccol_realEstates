package com.bbva.ccol.realestates.business.dto;


public enum StratumEnum {
	LOW_LOW("LOW_LOW"),
	LOW("LOW"),
	LOW_MEDIUM("LOW_MEDIUM"),
	MEDIUM("MEDIUM"),
	MEDIUM_HIGH("MEDIUM_HIGH"),
	HIGH("HIGH");
	
  private final String message;

	  StratumEnum(String message) {
	        this.message = message;
	    }

	    public String getMessage() {
	        return message;
	    }
}

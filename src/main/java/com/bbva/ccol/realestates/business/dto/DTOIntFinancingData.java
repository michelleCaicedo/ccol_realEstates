
package com.bbva.ccol.realestates.business.dto;




public class DTOIntFinancingData {

    public final static long serialVersionUID = 1L;
    private DTOIntAmount financingAmount;
    private DTOIntTerm terms;
    private DTOIntRate rates;
    private DTOIntAmount installmentAmount;
    private DTOIntIncome estimatedIncome;

    public DTOIntFinancingData() {
        //default constructor
    }

    public DTOIntAmount getFinancingAmount() {
        return financingAmount;
    }

    public void setFinancingAmount(DTOIntAmount financingAmount) {
        this.financingAmount = financingAmount;
    }

    public DTOIntTerm getTerms() {
        return terms;
    }

    public void setTerms(DTOIntTerm terms) {
        this.terms = terms;
    }

    public DTOIntRate getRates() {
        return rates;
    }

    public void setRates(DTOIntRate rates) {
        this.rates = rates;
    }

    public DTOIntAmount getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(DTOIntAmount installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public DTOIntIncome getEstimatedIncome() {
        return estimatedIncome;
    }

    public void setEstimatedIncome(DTOIntIncome estimatedIncome) {
        this.estimatedIncome = estimatedIncome;
    }

}

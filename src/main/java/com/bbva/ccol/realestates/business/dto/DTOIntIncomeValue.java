
package com.bbva.ccol.realestates.business.dto;




public class DTOIntIncomeValue {

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;
    private String frequency;

    public DTOIntIncomeValue() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

}

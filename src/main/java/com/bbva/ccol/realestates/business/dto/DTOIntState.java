
package com.bbva.ccol.realestates.business.dto;




public class DTOIntState {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;

    public DTOIntState() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

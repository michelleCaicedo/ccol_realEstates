
package com.bbva.ccol.realestates.business.dto;




public class DTOIntAmount {

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;

    public DTOIntAmount() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}

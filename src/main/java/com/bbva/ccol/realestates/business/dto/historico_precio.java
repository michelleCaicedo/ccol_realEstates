package com.bbva.ccol.realestates.business.dto;

import java.util.List;

public class historico_precio {

	private Double incremento;
	private List<precios> precios;
	
	public Double getIncremento() {
		return incremento;
	}
	public void setIncremento(Double incremento) {
		this.incremento = incremento;
	}
	public List<precios> getPrecios() {
		return precios;
	}
	public void setPrecios(List<precios> precios) {
		this.precios = precios;
	}
}

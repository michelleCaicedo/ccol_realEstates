package com.bbva.ccol.realestates.business.dto;

public class ZoneDetailMadiva {
	private String latitud;
	private String longitud;
	private String precisionGoogleGeometry;
	private String cp;
	private String via;
	private String numero;
	private String departamento;
	private String municipio;
	private String barrio;
	
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getPrecisionGoogleGeometry() {
		return precisionGoogleGeometry;
	}
	public void setPrecisionGoogleGeometry(String precisionGoogleGeometry) {
		this.precisionGoogleGeometry = precisionGoogleGeometry;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getBarrio() {
		return barrio;
	}
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

}

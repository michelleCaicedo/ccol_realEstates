
package com.bbva.ccol.realestates.business.dto;




public class DTOIntLocation {

    public final static long serialVersionUID = 1L;
    private String addressName;
    private DTOIntState state;
    private DTOIntGeographicGroup geographicGroup;
    private String zipCode;

    public DTOIntLocation() {
        //default constructor
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public DTOIntState getState() {
        return state;
    }

    public void setState(DTOIntState state) {
        this.state = state;
    }

    public DTOIntGeographicGroup getGeographicGroup() {
        return geographicGroup;
    }

    public void setGeographicGroup(DTOIntGeographicGroup geographicGroup) {
        this.geographicGroup = geographicGroup;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}

package com.bbva.ccol.realestates.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.ccol.realestates.business.ISrvIntRealEstates;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstateValuations;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstates;
import com.bbva.ccol.realestates.dao.RealEstatesDAO;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstates;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;


@Service
public class SrvIntRealEstates implements ISrvIntRealEstates {

	
	private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntRealEstates.class,"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	@Autowired
	RealEstatesDAO dao;
	
	@Override
	public List<DTOIntRealEstates> getRealEstates(EntryDataRealEstates entryDataRealEstates) {
		return dao.getRealEstates(entryDataRealEstates);
	}

		
	@Override
	public List<DTOIntRealEstateValuations> getRealEstateValuations(EntryDataRealEstateValuations entryDataRealEstateValuations) {
		return dao.getRealEstateValuations(entryDataRealEstateValuations);
	}

	

}


package com.bbva.ccol.realestates.business.dto;




public class DTOIntRealEstates {

    public final static long serialVersionUID = 1L;
    private String id;
    private String description;
    private DTOIntLocation location;
    private Double surface;
    private String imageUrl;
    private String propertyInformationWeb;
    private DTOIntRealEstateType realEstateType;
    private DTOIntDevelopmentType developmentType;
    private DTOIntAmount price;
    private DTOIntAmount priceByMeter;
    private Long parkingsNumber;
    private Long roomsNumber;
    private Long bathroomsNumber;
    private Long buildingAge;
    private DTOIntStratum stratum;
    private DTOIntPurposeType purposeType;
    private Double averageSurface;

    public DTOIntRealEstates() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DTOIntLocation getLocation() {
        return location;
    }

    public void setLocation(DTOIntLocation location) {
        this.location = location;
    }

    public Double getSurface() {
        return surface;
    }

    public void setSurface(Double surface) {
        this.surface = surface;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPropertyInformationWeb() {
        return propertyInformationWeb;
    }

    public void setPropertyInformationWeb(String propertyInformationWeb) {
        this.propertyInformationWeb = propertyInformationWeb;
    }

    public DTOIntRealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(DTOIntRealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public DTOIntDevelopmentType getDevelopmentType() {
        return developmentType;
    }

    public void setDevelopmentType(DTOIntDevelopmentType developmentType) {
        this.developmentType = developmentType;
    }

    public DTOIntAmount getPrice() {
        return price;
    }

    public void setPrice(DTOIntAmount price) {
        this.price = price;
    }

    public DTOIntAmount getPriceByMeter() {
        return priceByMeter;
    }

    public void setPriceByMeter(DTOIntAmount priceByMeter) {
        this.priceByMeter = priceByMeter;
    }

    public Long getParkingsNumber() {
        return parkingsNumber;
    }

    public void setParkingsNumber(Long parkingsNumber) {
        this.parkingsNumber = parkingsNumber;
    }

    public Long getRoomsNumber() {
        return roomsNumber;
    }

    public void setRoomsNumber(Long roomsNumber) {
        this.roomsNumber = roomsNumber;
    }

    public Long getBathroomsNumber() {
        return bathroomsNumber;
    }

    public void setBathroomsNumber(Long bathroomsNumber) {
        this.bathroomsNumber = bathroomsNumber;
    }

    public Long getBuildingAge() {
        return buildingAge;
    }

    public void setBuildingAge(Long buildingAge) {
        this.buildingAge = buildingAge;
    }

    public DTOIntStratum getStratum() {
        return stratum;
    }

    public void setStratum(DTOIntStratum stratum) {
        this.stratum = stratum;
    }

    public DTOIntPurposeType getPurposeType() {
        return purposeType;
    }

    public void setPurposeType(DTOIntPurposeType purposeType) {
        this.purposeType = purposeType;
    }

    public Double getAverageSurface() {
        return averageSurface;
    }

    public void setAverageSurface(Double averageSurface) {
        this.averageSurface = averageSurface;
    }

}

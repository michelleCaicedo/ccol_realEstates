
package com.bbva.ccol.realestates.business.dto;




public class DTOIntMode {

    public final static long serialVersionUID = 1L;
    private String id;
    private DTOIntIncomeValue name;

    public DTOIntMode() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntIncomeValue getName() {
        return name;
    }

    public void setName(DTOIntIncomeValue name) {
        this.name = name;
    }

}


package com.bbva.ccol.realestates.business.dto;




public class DTOIntRealEstatesSummaryItem {

    public final static long serialVersionUID = 1L;
    private DTOIntRealEstatesSummaryItemType realEstatesSummaryItemType;
    private Long value;

    public DTOIntRealEstatesSummaryItem() {
        //default constructor
    }

    public DTOIntRealEstatesSummaryItemType getRealEstatesSummaryItemType() {
        return realEstatesSummaryItemType;
    }

    public void setRealEstatesSummaryItemType(DTOIntRealEstatesSummaryItemType realEstatesSummaryItemType) {
        this.realEstatesSummaryItemType = realEstatesSummaryItemType;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}

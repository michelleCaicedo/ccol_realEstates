
package com.bbva.ccol.realestates.business.dto;




public class DTOIntRate {

    public final static long serialVersionUID = 1L;
    private DTOIntRateType rateType;
    private DTOIntMode mode;
    private DTOIntPercentage unit;

    public DTOIntRate() {
        //default constructor
    }

    public DTOIntRateType getRateType() {
        return rateType;
    }

    public void setRateType(DTOIntRateType rateType) {
        this.rateType = rateType;
    }

    public DTOIntMode getMode() {
        return mode;
    }

    public void setMode(DTOIntMode mode) {
        this.mode = mode;
    }

    public DTOIntPercentage getUnit() {
        return unit;
    }

    public void setUnit(DTOIntPercentage unit) {
        this.unit = unit;
    }

}

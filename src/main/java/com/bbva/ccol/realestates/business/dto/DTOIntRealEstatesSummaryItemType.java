
package com.bbva.ccol.realestates.business.dto;




public class DTOIntRealEstatesSummaryItemType {

    public final static long serialVersionUID = 1L;
    private String id;

    public DTOIntRealEstatesSummaryItemType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

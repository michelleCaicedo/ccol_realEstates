
package com.bbva.ccol.realestates.business.dto;




public class DTOIntIncome {

    public final static long serialVersionUID = 1L;
    private DTOIntIncomeType incomeType;
    private DTOIntIncomeValue incomeValues;

    public DTOIntIncome() {
        //default constructor
    }

    public DTOIntIncomeType getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(DTOIntIncomeType incomeType) {
        this.incomeType = incomeType;
    }

    public DTOIntIncomeValue getIncomeValues() {
        return incomeValues;
    }

    public void setIncomeValues(DTOIntIncomeValue incomeValues) {
        this.incomeValues = incomeValues;
    }

}

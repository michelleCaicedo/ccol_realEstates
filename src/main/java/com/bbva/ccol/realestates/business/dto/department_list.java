package com.bbva.ccol.realestates.business.dto;

import java.util.List;

public class department_list {
	private String comparables_number;
	private Double price; 
	private Double surface;
	private Double pricem2;
	private List<by_room> by_rooms;
	private List<comparable> comparables;
	
	public String getComparables_number() {
		return comparables_number;
	}
	public void setComparables_number(String comparables_number) {
		this.comparables_number = comparables_number;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getSurface() {
		return surface;
	}
	public void setSurface(Double surface) {
		this.surface = surface;
	}
	public Double getPricem2() {
		return pricem2;
	}
	public void setPricem2(Double pricem2) {
		this.pricem2 = pricem2;
	}
	public List<by_room> getBy_rooms() {
		return by_rooms;
	}
	public void setBy_rooms(List<by_room> by_rooms) {
		this.by_rooms = by_rooms;
	}
	public List<comparable> getComparables() {
		return comparables;
	}
	public void setComparables(List<comparable> comparables) {
		this.comparables = comparables;
	}
	
	
}

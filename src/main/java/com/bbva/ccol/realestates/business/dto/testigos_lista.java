package com.bbva.ccol.realestates.business.dto;

public class testigos_lista {	
	private String link;
	private String precio;
	private String metros2;
	private Long bathroom;
	private Long bedroom;
	private String descripcion;
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getMetros2() {
		return metros2;
	}
	public void setMetros2(String metros2) {
		this.metros2 = metros2;
	}
	public Long getBathroom() {
		return bathroom;
	}
	public void setBathroom(Long bathroom) {
		this.bathroom = bathroom;
	}
	public Long getBedroom() {
		return bedroom;
	}
	public void setBedroom(Long bedroom) {
		this.bedroom = bedroom;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}

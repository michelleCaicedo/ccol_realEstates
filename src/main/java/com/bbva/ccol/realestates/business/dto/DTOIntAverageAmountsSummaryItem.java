
package com.bbva.ccol.realestates.business.dto;




public class DTOIntAverageAmountsSummaryItem {

    public final static long serialVersionUID = 1L;
    private DTOIntAverageAmountsSummaryType averageAmountsSummaryType;
    private DTOIntAmount averageAmount;

    public DTOIntAverageAmountsSummaryItem() {
        //default constructor
    }

    public DTOIntAverageAmountsSummaryType getAverageAmountsSummaryType() {
        return averageAmountsSummaryType;
    }

    public void setAverageAmountsSummaryType(DTOIntAverageAmountsSummaryType averageAmountsSummaryType) {
        this.averageAmountsSummaryType = averageAmountsSummaryType;
    }

    public DTOIntAmount getAverageAmount() {
        return averageAmount;
    }

    public void setAverageAmount(DTOIntAmount averageAmount) {
        this.averageAmount = averageAmount;
    }

}

package com.bbva.ccol.realestates.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.ccol.realestates.business.dto.DTOIntRealEstateValuations;
import com.bbva.ccol.realestates.business.dto.DTOIntRealEstates;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstateValuations;
import com.bbva.ccol.realestates.facade.v01.dto.EntryDataRealEstates;
import com.bbva.ccol.realestates.facade.v01.mapper.Mapper;
import com.bbva.ccol.realestates.properties.RealEstatesProperties;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.rest.IProxyRestConnector;
import com.bbva.jee.arq.spring.core.rest.RestConnectorFactory;
import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.rest.RestConnectorType;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

@Component
public class RealEstatesDAOImpl  implements RealEstatesDAO {

	@Autowired
	RestConnectorFactory restConnectorFactory;
	
	private static I18nLog log = I18nLogFactory.getLogI18n(RealEstatesDAOImpl.class,"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	RestConnectorResponse gResponse = new RestConnectorResponse();
	
	@Resource(name = "realEstatesProperties")
	private RealEstatesProperties realEstatesProperties;
	
	@Override
	public List<DTOIntRealEstates> getRealEstates(EntryDataRealEstates entryDataRealEstates) {
		List<DTOIntRealEstates> listDTOIntRealEstates =new ArrayList<DTOIntRealEstates>();
		
		String urlMadiva = "";
		String parameters;
		String service=Mapper.validateService(entryDataRealEstates);
		
		if(!service.equalsIgnoreCase("validateAddressName")){
			parameters= Mapper.dtoIntRealEstateToJsonInfoZona(entryDataRealEstates);
			urlMadiva=realEstatesProperties.getProperty("url.madiva.informeZona")+"?"+parameters;
			listDTOIntRealEstates=Mapper.dtoExtToDtoIntListRealEstatesInfoZone(Mapper.jsonToListRealEstates(launchMadivaService(urlMadiva)));
		}	
		
		if(service.equalsIgnoreCase("validateAddressName")){
			parameters= Mapper.dtoIntRealEstateToJsonValDir(entryDataRealEstates);
			urlMadiva=realEstatesProperties.getProperty("url.madiva.validarDireccion")+"?"+parameters;
			 listDTOIntRealEstates=Mapper.dtoExtToDtoIntListRealEstatesValDir(Mapper.jsonToZoneDetailMadiva(launchMadivaService(urlMadiva)));
		}
		return listDTOIntRealEstates;
	}

		
	@Override
	public List<DTOIntRealEstateValuations> getRealEstateValuations(EntryDataRealEstateValuations entryDataRealEstateValuations) {
		 String urlMadiva = "";
		 List<DTOIntRealEstateValuations> listDTOIntRealEstatesValuations= new ArrayList<DTOIntRealEstateValuations>();
		 String parameters=Mapper.parameterRealEstateValuations(entryDataRealEstateValuations);
		 urlMadiva=realEstatesProperties.getProperty("url.madiva.valoracion")+"?"+parameters;
		 listDTOIntRealEstatesValuations= Mapper.dtoExtToDtoIntListRealEstateValuations(Mapper.jsonToListRealEstateValuations(launchMadivaService(urlMadiva)));
		 return listDTOIntRealEstatesValuations;
		 
	}
	
		public String launchMadivaService(String urlMadiva){
			HashMap<String, String> optionalHeaders = new HashMap<String, String>();
			IProxyRestConnector iRestConnector = (IProxyRestConnector) restConnectorFactory.getRestConnector(RestConnectorType.BASIC);			String json;			
			gResponse = iRestConnector.doGet(urlMadiva, null, optionalHeaders);
			json = gResponse.getResponseBody();
				if (gResponse.getStatusCode() != 200) {
					throw new BusinessServiceException(json ,"" );
				}
			return json;
		}
	
}

